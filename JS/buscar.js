$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");

	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
		/*PONGA AQUI SU CÓDIGO*/
		var miLista = $("#miLista");
		var listaPeliculas = $("#lista-pelis");
		//https://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query=dsa
		$.ajax({
			//url: "https://randomuser.me/api/?results=8&nat=au,fr,es&inc=gender,name,picture,email,nat",
			url: "https://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query="+palabra ,
			success: function(respuesta) {
				
				setTimeout(function () {
					console.log(respuesta);
					listaPeliculas.empty();
					$.each(respuesta.results, function(index, elemento) {

						miLista.append('<li>'+elemento.title+' </li>');
			
					});
					listaPeliculas.slideDown("slow");
				}, 3000);

			},
			error: function() {
				console.log("No se ha podido obtener la información");
			},
			beforeSend: function() { 
				console.log('CARGANDO');
				listaPeliculas.empty();
				listaPeliculas.append('<div class="text-center"><img src="images/loading.gif" /></div>');
			},
		});
	});

});


